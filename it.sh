#!/bin/sh -x

echo "=== setup ==="
docker build -t fizzbuzz/api api
docker build -t fizzbuzz/db db
docker build -t fizzbuzz/api-it-client api-it-client

docker run -d --name db -e POSTGRES_PASSWORD=password fizzbuzz/db
docker run -d --name api --link db -e PGHOST=db fizzbuzz/api

echo "=== logs: it ==="
docker run --rm --name api-it-client --link api fizzbuzz/api-it-client && RV=0 || RV=1

echo "=== logs: db ==="
docker logs db

echo "=== logs: api ==="
docker logs api

echo "=== cleanup ==="
docker rm -f api db

exit $RV
