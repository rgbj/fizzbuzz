CREATE USER fizzbuzz_u WITH PASSWORD 'fizzbuzz_p';
CREATE DATABASE fizzbuzz_d OWNER fizzbuzz_u ENCODING 'UTF8' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;

\connect fizzbuzz_d fizzbuzz_u

CREATE TABLE public.fizzbuzz_frequency (
    created timestamptz default now(),
    fblimit integer not null,
    fizzdiv integer not null,
    fizzmsg text not null,
    buzzdiv integer not null,
    buzzmsg text not null
    );
