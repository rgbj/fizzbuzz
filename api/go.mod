module fizzbuzz/api

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/labstack/gommon v0.3.0
	github.com/lib/pq v1.8.0
	github.com/stretchr/testify v1.6.1
)
