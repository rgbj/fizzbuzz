package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"

	"github.com/labstack/gommon/log"
)

var pghost = "localhost"
var cs = "host=%s port=5432 user=fizzbuzz_u password=fizzbuzz_p dbname=fizzbuzz_d sslmode=disable"

// LogRequest stores in a db table a trace of a request: parameters + timestamp
func LogRequest(req FizzBuzzReq) {
	db, err := sql.Open("postgres", fmt.Sprintf(cs, pghost))
	if err != nil {
		log.Error(err)
		return
	}
	defer db.Close()
	_, err = db.Exec(`INSERT INTO public.fizzbuzz_frequency 
	(fblimit, fizzdiv, fizzmsg, buzzdiv, buzzmsg) 
	VALUES ($1, $2, $3, $4, $5)`,
		req.Limit,
		req.FizzDiv,
		req.FizzMsg,
		req.BuzzDiv,
		req.BuzzMsg)
	if err != nil {
		log.Error(err)
	}
}

// GetTopRequest returns the request most commonely encountered
func GetTopRequest() (*FizzBuzzReq, int, error) {
	db, err := sql.Open("postgres", fmt.Sprintf(cs, pghost))
	if err != nil {
		return nil, -1, err
	}
	defer db.Close()
	query := `SELECT COUNT(*), fblimit, fizzdiv, fizzmsg, buzzdiv, buzzmsg 
	FROM public.fizzbuzz_frequency 
	GROUP BY fblimit, fizzdiv, fizzmsg, buzzdiv, buzzmsg 
	ORDER BY count DESC LIMIT 1`
	var count int
	var r FizzBuzzReq
	err = db.QueryRow(query).Scan(&count, &r.Limit, &r.FizzDiv, &r.FizzMsg, &r.BuzzDiv, &r.BuzzMsg)
	if err != nil {
		return nil, -1, err
	}
	return &r, count, nil
}
