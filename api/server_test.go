package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVersionRoute(t *testing.T) {
	router := setupRouter()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/version", nil)
	if err != nil {
		t.Error(err)
	}
	router.ServeHTTP(w, req)
	r := w.Result()

	assert.Equal(t, 200, r.StatusCode)
	assert.Equal(t, []string{"application/json; charset=utf-8"}, r.Header["Content-Type"])
	type Version struct {
		Version string `json:"version"`
	}
	var version Version
	json.NewDecoder(r.Body).Decode(&version)
	assert.Equal(t, "0.0.1", version.Version)
}

func TestFizzBuzzRoute(t *testing.T) {
	rjb, err := json.Marshal(map[string]interface{}{
		"limit":   10,
		"fizzdiv": 3,
		"fizzmsg": "Fizz",
		"buzzdiv": 5,
		"buzzmsg": "Buzz",
	})
	if err != nil {
		t.Error(err)
	}

	router := setupRouter()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/fizzbuzz", bytes.NewReader(rjb))
	if err != nil {
		t.Error()
	}
	router.ServeHTTP(w, req)
	r := w.Result()

	assert.Equal(t, 200, r.StatusCode)
	assert.Equal(t, []string{"application/json; charset=utf-8"}, r.Header["Content-Type"])
	type fizzBuzzResp struct {
		FizzBuzzReq
		Serie []interface{} `json:"serie" binding:"required"`
	}
	expected := fizzBuzzResp{FizzBuzzReq{10, 3, "Fizz", 5, "Buzz"},
		[]interface{}{
			float64(1),
			float64(2),
			"Fizz",
			float64(4),
			"Buzz",
			"Fizz",
			float64(7),
			float64(8),
			"Fizz",
			"Buzz",
		}}
	var resp fizzBuzzResp
	json.NewDecoder(r.Body).Decode(&resp)

	assert.Equal(t, expected, resp)
}
