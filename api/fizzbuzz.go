package main

// FizzBuzz computes a generalized fizz buzz series from 1 to `limit`.
// THe original fizzbuzz well-known function uses
// `fizzdiv = 3`, `fizzmsg = "Fizz"`, `buzzdiv = 5`, `buzzmsg = "Buzz"`
// It returns a slice of either `int` or `string`, hence the use of the
// empty interface{}.
func FizzBuzz(
	limit int,
	fizzdiv int, fizzmsg string,
	buzzdiv int, buzzmsg string) []interface{} {
	results := make([]interface{}, limit)
	for i := 1; i <= limit; i++ {
		result := ""
		if i%fizzdiv == 0 {
			result += fizzmsg
		}
		if i%buzzdiv == 0 {
			result += buzzmsg
		}
		if result != "" {
			results[i-1] = result
		} else {
			results[i-1] = i
		}
	}
	return results
}
