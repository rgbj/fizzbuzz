package main

import "testing"

// ensure regular FizzBuzz works as expected
func TestFizzBuzz_3_Fizz_5_Buzz(t *testing.T) {
	expected := []interface{}{
		1, 2, "Fizz", 4, "Buzz",
		"Fizz", 7, 8, "Fizz", "Buzz",
		11, "Fizz", 13, 14, "FizzBuzz",
		16, 17, "Fizz", 19, "Buzz",
		"Fizz", 22, 23, "Fizz", "Buzz",
		26, "Fizz", 28, 29, "FizzBuzz",
		31, 32, "Fizz", 34, "Buzz",
	}

	for i, actual := range FizzBuzz(35, 3, "Fizz", 5, "Buzz") {
		if actual != expected[i] {
			t.Errorf("i: %d, got: %s, want: %s.", i, actual, expected[i])
		}
	}
}

// ensure generalized FizzBuzz works as expected
func TestFizzBuzz_6_y_1_x(t *testing.T) {
	expected := []interface{}{
		"x", "x", "x", "x", "x", "yx",
		"x", "x", "x", "x", "x", "yx",
		"x", "x", "x", "x", "x", "yx",
		"x", "x", "x", "x", "x", "yx",
		"x", "x", "x", "x", "x", "yx",
		"x", "x", "x", "x", "x", "yx",
	}

	for i, actual := range FizzBuzz(36, 6, "y", 1, "x") {
		if actual != expected[i] {
			t.Errorf("i: %d, got: %s, want: %s.", i, actual, expected[i])
		}
	}
}
func TestFizzBuzz_30_F_40_B(t *testing.T) {
	expected := []interface{}{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for i, actual := range FizzBuzz(10, 30, "F", 40, "B") {
		if actual != expected[i] {
			t.Errorf("i: %d, got: %s, want: %s.", i, actual, expected[i])
		}
	}
}
func TestFizzBuzz_6_F_4_B(t *testing.T) {
	expected := []interface{}{
		1, 2, 3, "B", 5, "F", 7, "B", 9, 10,
		11, "FB", 13, 14, 15, "B", 17, "F", 19, "B",
		21, 22, 23, "FB", 25, 26, 27, "B", 29, "F",
		31, "B", 33, 34, 35, "FB", 37, 38, 39, "B",
		41, "F", 43, "B", 45, 46, 47, "FB", 49, 50,
	}

	for i, actual := range FizzBuzz(50, 6, "F", 4, "B") {
		if actual != expected[i] {
			t.Errorf("i: %d, got: %s, want: %s.", i, actual, expected[i])
		}
	}
}
