package main

import (
	"database/sql"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func version(c *gin.Context) {
	c.JSON(200, gin.H{"version": "0.0.1"})
}

// FizzBuzzReq represents a generalized FizzBuzz query parameters
type FizzBuzzReq struct {
	Limit   int    `json:"limit" binding:"required"`
	FizzDiv int    `json:"fizzdiv" binding:"required"`
	FizzMsg string `json:"fizzmsg" binding:"required"`
	BuzzDiv int    `json:"buzzdiv" binding:"required"`
	BuzzMsg string `json:"buzzmsg" binding:"required"`
}

func fizzbuzz(c *gin.Context) {
	// parse request as JSON, syntactic validation
	var req FizzBuzzReq
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// semantic validation
	if req.Limit < 0 || req.Limit > 10000 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "limit out of range 0 - 10000"})
		return
	}
	if req.FizzDiv < 1 || req.FizzDiv > 10000 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "fizzdiv out of range 1 - 10000"})
		return
	}
	if len(req.FizzMsg) > 10000 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "fizzmsg too long"})
		return
	}
	if req.BuzzDiv < 1 || req.BuzzDiv > 10000 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "buzzdiv out of range 1 - 10000"})
		return
	}
	if len(req.BuzzMsg) > 10000 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "buzzmsg too long"})
		return
	}
	// log the request for stats
	LogRequest(req)
	// the input looks well-formed, do the work
	serie := FizzBuzz(
		req.Limit,
		req.FizzDiv, req.FizzMsg,
		req.BuzzDiv, req.BuzzMsg)
	// format the response
	c.JSON(200, gin.H{
		"limit":   req.Limit,
		"fizzdiv": req.FizzDiv,
		"fizzmsg": req.FizzMsg,
		"buzzdiv": req.BuzzDiv,
		"buzzmsg": req.BuzzMsg,
		"serie":   serie,
	})
}

type statsResp struct {
	Request *FizzBuzzReq `json:"request"`
	Count   int          `json:"count" binding:"required"`
}

func stats(c *gin.Context) {
	req, count, err := GetTopRequest()
	if err != nil && err != sql.ErrNoRows {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "statistics unavailable"})
		return
	}
	c.JSON(http.StatusOK, statsResp{Request: req, Count: count})
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	// version, the simpliest endpoint there is: returns a static string
	// encoding the service version
	r.GET("/version", version)
	// ready & alive: should be used as kubernetes probes
	r.GET("/ready", version)
	r.GET("/alive", version)
	// fizzbuzz: the actual service
	r.POST("/fizzbuzz", fizzbuzz)
	// stats endpoint
	r.GET("/stats", stats)
	return r
}

func main() {
	if v, ok := os.LookupEnv("PGHOST"); ok {
		pghost = v
	}

	r := setupRouter()
	r.Run(":8080")
}
