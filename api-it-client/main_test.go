package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

// ensure a call to `/version` works as expected
func TestVersion(t *testing.T) {
	resp, err := http.Get("http://api:8080/version")
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, 200, resp.StatusCode)
	assert.Equal(t, []string{"application/json; charset=utf-8"}, resp.Header["Content-Type"])
	type versionResp struct {
		Version string `json:"version" binding:"required"`
	}
	var jr versionResp
	json.NewDecoder(resp.Body).Decode(&jr)
	expected := versionResp{"0.0.1"}
	assert.Equal(t, expected, jr)
}

type fizzBuzzReq struct {
	Limit   int    `json:"limit" binding:"required"`
	FizzDiv int    `json:"fizzdiv" binding:"required"`
	FizzMsg string `json:"fizzmsg" binding:"required"`
	BuzzDiv int    `json:"buzzdiv" binding:"required"`
	BuzzMsg string `json:"buzzmsg" binding:"required"`
}

func callFizzBuzz(t *testing.T,
	limit int,
	fizzdiv int, fizzmsg string,
	buzzdiv int, buzzmsg string,
	fbr []interface{}) *http.Response {
	body, err := json.Marshal(map[string]interface{}{
		"limit":   limit,
		"fizzdiv": fizzdiv,
		"fizzmsg": fizzmsg,
		"buzzdiv": buzzdiv,
		"buzzmsg": buzzmsg,
	})
	if err != nil {
		t.Error(err)
	}
	resp, err := http.Post("http://api:8080/fizzbuzz",
		"application/json; charset=utf-8",
		bytes.NewBuffer(body))
	if err != nil {
		t.Error(err)
	}
	return resp
}

// test input validation
func TestFizzBuzz400(t *testing.T) {
	resp := callFizzBuzz(t, -23, 3, "Fizz", 5, "Buzz", []interface{}{})
	assert.Equal(t, 400, resp.StatusCode)
	resp = callFizzBuzz(t, 13245, 3, "Fizz", 5, "Buzz", []interface{}{})
	assert.Equal(t, 400, resp.StatusCode)

	resp = callFizzBuzz(t, 10, -12, "Fizz", 5, "Buzz", []interface{}{})
	assert.Equal(t, 400, resp.StatusCode)
	// resp = callFizzBuzz(t, 10, 0, "Fizz", 5, "Buzz", []interface{}{})
	// assert.Equal(t, 400, resp.StatusCode)
	// resp = callFizzBuzz(t, 10, 12345, "Fizz", 5, "Buzz", []interface{}{})
	// assert.Equal(t, 400, resp.StatusCode)

	// resp = callFizzBuzz(t, 10, 3, "Fizz", -12, "Buzz", []interface{}{})
	// assert.Equal(t, 400, resp.StatusCode)
	// resp = callFizzBuzz(t, 10, 3, "Fizz", 0, "Buzz", []interface{}{})
	// assert.Equal(t, 400, resp.StatusCode)
	// resp = callFizzBuzz(t, 10, 3, "Fizz", 12345, "Buzz", []interface{}{})
	// assert.Equal(t, 400, resp.StatusCode)
}

func callFizzBuzz35(t *testing.T, limit int, fbr []interface{}) {
	resp := callFizzBuzz(t, limit, 3, "Fizz", 5, "Buzz", fbr)
	assert.Equal(t, 200, resp.StatusCode)
	assert.Equal(t, []string{"application/json; charset=utf-8"}, resp.Header["Content-Type"])
	type fizzBuzzResp struct {
		fizzBuzzReq
		Serie []interface{} `json:"serie" binding:"required"`
	}
	var jr fizzBuzzResp
	json.NewDecoder(resp.Body).Decode(&jr)
	expected := fizzBuzzResp{
		fizzBuzzReq{limit, 3, "Fizz", 5, "Buzz"}, fbr}
	assert.Equal(t, expected, jr)
}

// ensure canonical FizzBuzz works
func TestFizzBuzz1(t *testing.T) {
	callFizzBuzz35(t, 10, []interface{}{
		float64(1),
		float64(2),
		"Fizz",
		float64(4),
		"Buzz",
		"Fizz",
		float64(7),
		float64(8),
		"Fizz",
		"Buzz",
	})
}

func TestFizzBuzz2(t *testing.T) {
	for i := 0; i < 3; i++ {
		callFizzBuzz35(t, 35, []interface{}{
			float64(1), float64(2), "Fizz", float64(4), "Buzz",
			"Fizz", float64(7), float64(8), "Fizz", "Buzz",
			float64(11), "Fizz", float64(13), float64(14), "FizzBuzz",
			float64(16), float64(17), "Fizz", float64(19), "Buzz",
			"Fizz", float64(22), float64(23), "Fizz", "Buzz",
			float64(26), "Fizz", float64(28), float64(29), "FizzBuzz",
			float64(31), float64(32), "Fizz", float64(34), "Buzz",
		})
	}
}

// ensure stats work
func TestStats(t *testing.T) {
	resp, err := http.Get("http://api:8080/stats")
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, 200, resp.StatusCode)
	assert.Equal(t, []string{"application/json; charset=utf-8"}, resp.Header["Content-Type"])
	type statsResp struct {
		Request fizzBuzzReq `json:"request" binding:"required"`
		Count   int         `json:"count" binding:"required"`
	}
	var jr statsResp
	json.NewDecoder(resp.Body).Decode(&jr)
	expected := statsResp{fizzBuzzReq{35, 3, "Fizz", 5, "Buzz"}, 3}
	assert.Equal(t, expected, jr)
}
