# Input

Write a simple fizz-buzz REST server. 

The original fizz-buzz consists in writing all numbers from 1 to 100, and just replacing all multiples of 3 by "fizz", all multiples of 5 by "buzz", and all multiples of 15 by "fizzbuzz". The output would look like this: "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,...".

Your goal is to implement a web server that will expose a REST API endpoint that: 
Accepts five parameters : three integers int1, int2 and limit, and two strings str1 and str2.
Returns a list of strings with numbers from 1 to limit, where: all multiples of int1 are replaced by str1, all multiples of int2 are replaced by str2, all multiples of int1 and int2 are replaced by str1str2.

The server needs to be:
- Ready for production
- Easy to maintain by other developers
 
Bonus Question :
- Add a statistics endpoint allowing users to know what the most frequent request has been. 
This endpoint should:
- Accept no parameter
- Return the parameters corresponding to the most used request, as well as the number of hits for this request

# choices & assumptions

- The API consumes & produces JSON.
- It is implemented using the [Gin](https://gin-gonic.com) framework.
- The code designed to produce docker containers for kubernetes deployment.
- It uses a postgresql database to store the data required for stats. It should work without the database, however -- but then, no more stats.

# limitations & shortcuts

- the database connection parameters aren't configurable like they should be: Only the hostname can be configured at the moment, through the `PGHOST` environment variable.
- the code makes use of the empty interface in order to return either a number or a string in the JSON response. Since FizzBuzz is quite a synthetic example, I can't really foresee what other code would make use of that type and thus how to better encode it. Thus, I chose to use the simplest solution for the problem at hand, even though it might not be canonically "clean", type-wise.

# API description

There are five endpoints:
- `GET /version` returns the service version: `{"version": "0.0.1"}`
- `GET /ready` and `GET /alive` are intended as probes for kubernetes. They should return HTTP 200 when the service is ready to handle request (resp. healthy), 400 otherwise. Currently, because this is a toy service, they're actually identicall to the `GET /version` endpoint.
- `POST /fizzbuzz` renders the fizzbuzz computation service. It takes a JSON body:

```json
{
    "limit": 20,
    "fizzdiv": 3,
    "fizzmsg": "Fizz",
    "buzzdiv": 5,
    "buzzmsg": "Buzz"
}
```
and returns:
```json
{
    "limit": 20,
    "fizzdiv": 3,
    "fizzmsg": "Fizz",
    "buzzdiv": 5,
    "buzzmsg": "Buzz",
    "serie": [1,2,"Fizz",4,"Buzz","Fizz",7,8,"Fizz","Buzz",11,"Fizz",13,14,"FizzBuzz",16,17,"Fizz",19,"Buzz"]
}
```
Input validation will reject any int over 10000.
- `GET stats` returns stats about the most served request. It returns something that looks like:
```json
{
    "count": 4,
    "request": {
        "buzzdiv": 5,
        "buzzmsg": "Buzz",
        "fizzdiv": 3,
        "fizzmsg": "Fizz",
        "limit": 35
    }
}
```
In case of a tie, any of the top requests could be the winner.

# Dev notes

## repository organisation

- `api` contains the actual service code with its unit tests
- `api-it-client` is a test client only used to integration-test the service.

## how to unit-test

Prerequisistes:
- A working `go` installation. I am using 1.15, I didn't test any lower versions.

From the `api` directory, run `go test`.

## how to integration-test

Prerequisistes:
- `docker`
- `bash`

Run the `it.sh` script found in the top-level directory of this reposotory.

## How to run

Prerequisistes:
- `go`

From the `api` directory, run `go run server.go`. Hit the server on port `8080`.

## how to build

Prerequisistes:
- `docker`
- `bash`
- `skaffold` (optional)

From the top-level directory, run `build.sh`. Alternatively, run `skaffold build`.

## niceties

### efficient dev loop

Prerequisistes:
- `skaffold`
- `kubernetes`

Use `skaffold dev --port-forward` to watch sources, build & deploy when changes get spotted.

### unit tests & integration tests in gitlab

Pushing this repository to gitlab should start a pipeline composed of two jobs, `ut` running unit tests and `it` running integration tests.
